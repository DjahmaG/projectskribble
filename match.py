

class Match:

    def __init__(self, contestants=None):
        if contestants is None:
            contestants = []
        self.contestants = contestants
        self.scores = {}
        self.berserk = {}
        for c in self.contestants:
            self.berserk[c] = False

    def add_contestant(self, contestant, berserk=False):
        if self.contestants.__contains__(contestant):
            return contestant.get_name() + " was already added."
        else:
            self.contestants.append(contestant)
            self.berserk[contestant.id] = False
            return contestant.get_name() + " added."

    def set_berserk(self, ids):
        for c in self.berserk:
            if c.id in ids:
                self.berserk[c.id] = True

    def add_berserker(self, id):
        self.berserk[id] = True
        print("Berserker add")

    def amount_of_contestants(self):
        return len(self.contestants)
    
    def enter_scores(self, scores_list=None):
        if scores_list is None:
            for c in self.contestants:
                print(c.id + " - " + c.get_name() + ": ", end='')
                self.scores[c.id] = int(input().strip())
        else:
            i = 0
            for c in self.contestants:
                self.scores[c.id] = int(scores_list[i])
                print(c.id + " - " + c.get_name() + ": " + scores_list[i] + "\n", end='')
                i +=1
        print('\n \033[92m' + "calculating new ratings..." + '\033[0m' + '\n')
        self.calculate_new_elo()

    def calculate_new_elo(self):
        scores = {}
        for c in self.contestants:
            scores[c.id] = 0
            for o in self.contestants:
                if c.id == o.id:
                    continue
                if self.scores[c.id] > self.scores[o.id]:
                    wdl = 1
                elif self.scores[c.id] == self.scores[o.id]:
                    wdl = 0.5
                else:
                    wdl = 0
                ctw = 1 / (1 + pow(10, (o.rating - c.rating)/400))
                ctwp = round(ctw * 10000) / 100
                tmp = min(abs(int((self.scores[c.id] - self.scores[o.id]) * 0.032)) + 1, 32) * (wdl - ctw)
                score = round(tmp)
                Match.print_result(c, o, score, ctwp)
                scores[c.id] += score
            print("")
        for c in self.contestants:
            self.print_total_gain(c, scores[c.id])
            if self.berserk[c.id]:
                scores[c.id] *= 2
            c.rating += scores[c.id]

    def print_total_gain(self, c, score):
        print(c.get_name_with_title_and_spacing(17) + " ", end='')
        print(Match.colored_signed_number(score), end='')
        if self.berserk[c.id]:
            print("\033[36m x2\033[0m", end='')
        print("")

    @staticmethod
    def colored_signed_number(n):
        if n < 0:
            return '\033[91m' + str(n) + '\033[0m'
        elif n > 0:
            return '\033[92m' + "+" + str(n) + '\033[0m'
        else:
            return '\033[90m' + "+" + str(n) + '\033[0m'

    @staticmethod
    def print_result(c, o, point_gain, ctwp):
        print(c.get_name_with_title() + " vs " + o.get_name_with_title() + ": "
              + Match.colored_signed_number(point_gain) + " voor " + c.get_name()
              + " (ctw: " + str(ctwp) + "%)")

    def print_scores(self):
        print('\033[21m' + '\033[30m' + "Id;Naam:" + '\033[0m')
        for c in self.contestants:
            print(c.id + " - " + c.get_name() + ": " + str(self.scores[c.id]))

    def print_contestants(self):
        #print(self.berserk)
        print('\033[21m' + '\033[30m' + " {:7}{:16}{:}:".format("Id", "Name", "Rating") + '\033[0m')
        for c in self.contestants:
            print(" " + str(c.id) + ": " + c.get_name_with_title_and_spacing() + str(c.rating), end='')
            if self.berserk[c.id]:
                print('\033[91m' + "\tB" + '\033[0m', end='')
            print("")

