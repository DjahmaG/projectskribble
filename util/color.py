COLOR_RESET = "\033[0m"


def color_string_from_list(string, color_code_list):
    colored_string = ""
    for color_code in color_code_list:
        colored_string += "\033[" + str(color_code) + "m"

    colored_string += string
    colored_string += COLOR_RESET

    return colored_string
