from enum import Enum

from util.color import color_string_from_list


class Title:

    def __init__(self, title, long_title, rating, color_codes=None):
        if color_codes is None:
            color_codes = []
        self.title = title
        self.long_title = long_title
        self.rating = rating
        self.__color_codes = color_codes

    def get_title_with_color(self):
        return color_string_from_list(self.title, self.__color_codes)

    def get_full_title_with_color(self):
        return color_string_from_list(self.long_title, self.__color_codes)


class Titles(Enum):
    GM = Title("GM", "Grandmaster", 2000, [95, 1])
    SM = Title("SM", "Skribbl Master", 1750, [91, 1])
    CM = Title("CM", "Candidate Master", 1600, [96, 1])

    @staticmethod
    def get_title(rating):
        for title in Titles:
            if rating >= title.value.rating:
                return title.value
        return None

