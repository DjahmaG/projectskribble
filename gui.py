import tkinter as tk
import os
import functools as ft

from loop import Main
from contender import Contender
from match import Match


def start():
    global main
    global match
    global klassement
    global klassement_temp
    
    #clear terminal
    os.system("cls")
    
    #open csv file
    f = open("ratings.csv", "r")
    f.readline()
    
    klassement = []
    klassement_temp = {}
    
    for x in f:
        (id, name, rating, colorcodes) = x.split(';')
        #print(f.readline())
        klassement_temp[id] = (Contender(id, name, rating, colorcodes))
    
    for key in klassement_temp:
        klassement.append(None)
    
    for key in klassement_temp:
        klassement[int(key)] = klassement_temp[key]
    
    f.close()
    
    main = Main()
    match = Match()
    main.klassement = klassement


   
def start_window():
    ############# Configuratie #############
    master = tk.Tk()
    
    master.title("Skribble is cool!")                               #rename title of window
    master.resizable(0, 0)                                          #Make not resizable
    
    master.geometry('400x500+750+300')
    master.configure(bg = "light blue")                            #background color
    
    ############# Buttons #############
    button_w = 15
    button_h = 3
    #bg_color = "light blue"
    
    leaderboard = tk.Button(master, 
        text = "Leaderboard",
        width = button_w,
        height = button_h,
        fg = "black",
        command = lambda: main.print_klassement()
    )
    
    new_match = tk.Button(master, 
        text = "New match",
        width = button_w,
        height = button_h,
        fg = "black",
        command = n_match
    )
    
    player_set = tk.Button(master, 
        text = "Player settings",
        width = button_w,
        height = button_h,
        fg = "black",
        command = player_settings
    )
    
    titles = tk.Button(master, 
        text = "Titles",
        width = button_w,
        height = button_h,
        fg = "black",
        command = lambda: main.print_titles()
    )
    
    exit_button = tk.Button(master, 
        text = "Exit!",
        width = button_w,
        height = button_h,
        bg = "tomato2",
        fg = "black",
        command = lambda:master.destroy()
    )
    
    ############# place #############
    leaderboard.place(relx=0.5, rely=(0.9/5)-0.05, anchor="center")
    new_match.place(relx=0.5, rely=(2*(0.9/5))-0.05, anchor="center")
    player_set.place(relx=0.5, rely=(3*(0.9/5))-0.05, anchor="center")
    titles.place(relx=0.5, rely=(4*(0.9/5))-0.05, anchor="center")
    exit_button.place(relx=0.5, rely=(0.9)-0.05, anchor= "center")
    
    master.mainloop()



def n_match():
    ############# Configuratie #############
    global match_w
    match_w = tk.Tk()
    
    match_w.title("New Match!")                               #rename title of window
    match_w.resizable(0, 0)                                   #Make not resizable
    match_w.geometry("+200+10")
    
    #match_w.geometry('400x500+1150+300')
    match_w.configure(bg = "light blue")  
    
    ############# button functions #############
    def set_box(box):
        if(states[box].get() == 0):
            states[box].set(1)
        else:
            states[box].set(0)
        
    def check_states(checkboxes):
        for i in range(len(checkboxes)):
            if(states[i].get()):
                main.match.add_berserker(str(i))
                
    def update_scores(entries):
        scores = []
        id_list = []
        
        for i in range(len(entries)):
            if(len(entries[i].get()) != 0):
                #print(i)
                scores.append(entries[i].get())
                id_list.append(str(i))
        main.start_match(id_list)
        check_states(checkboxes)
        
        main.end_match(scores)
        main.match.print_contestants()
        save_match_window()

    ############# Labels, entries and button #############
    id_lbl = tk.Label(match_w,text = "ID", width = 5,fg = "black", bg = "light blue")
    name_lbl = tk.Label(match_w,text = "Name", fg = "black", bg = "light blue")
    score_lbl = tk.Label(match_w,text = "Score", fg = "black", bg = "light blue")
    berserk_lbl = tk.Label(match_w,text = "Berserker", fg = "black", bg = "light blue")
    
    id_labels = []
    name_labels = []
    entries = []
    checkboxes = []
    states = [] 
    
   
    for key, value in klassement_temp.items():
        id_labels.append(tk.Label(match_w, text = key, fg = "black", bg = "light blue"))
        name_labels.append(tk.Label(match_w, text = value.name, fg = "black", bg = "light blue"))
        entries.append(tk.Entry(match_w))
        
        var = tk.IntVar()
        states.append(var)
        checkboxes.append(tk.Checkbutton(match_w,variable=states[int(key)], onvalue=1, offvalue=0, bg = "light blue", command = ft.partial(set_box,int(key))))
    
    button= tk.Button(match_w, text = "OK", width = 6,height = 1, fg = "black", bg = "SpringGreen2", command = lambda: update_scores(entries))
    exit_button = tk.Button(match_w, text = "Exit", width = 6,height = 1, fg = "black", bg = "tomato2", command = lambda: match_w.destroy())
    
    ############# grid #############
    id_lbl.grid(row = 0, column = 0)
    name_lbl.grid(row = 0, column = 1)
    score_lbl.grid(row = 0, column = 2)
    berserk_lbl.grid(row = 0, column = 3)
    
    aantal = 1
    col = 0
    for i in range(1,len(id_labels)+1):
        if(aantal == 16):
            aantal = 1
            col += 4
        id_labels[i-1].grid(row = aantal, column = 0+col, padx = 5, pady = 2, sticky = "w")
        name_labels[i-1].grid(row = aantal, column = 1+col, padx = 5, pady = 2, sticky = "w")
        entries[i-1].grid(row = aantal, column = 2+col, padx = 5, pady = 2)
        checkboxes[i-1].grid(row = aantal, column = 3+col,  pady = 2)
        
        aantal += 1 
    button.grid(row = 16, column = 3+col, padx = 5, pady = 2, sticky = "e")
    exit_button.grid(row = 16, column = 2+col, padx = 5, pady = 2, sticky = "e")
    match_w.mainloop()
    
def save_match_window():
    ############# Configuratie #############
    save_match_w = tk.Tk()
    
    save_match_w.title("Save match")                               #rename title of window
    save_match_w.resizable(0, 0)                                   #Make not resizable
    save_match_w.geometry("400x100+300+150")
    save_match_w.configure(bg = "light blue") 
    
    ############# functions #############
    def destroy():
        main.cancel_match()
        save_match_w.destroy()
        start()
        
    def save():
        main.save_results()
        save_match_w.destroy()
        match_w.destroy()
        #start()
        
    ############# labels and buttons #############
    lbl = tk.Label(save_match_w,text = "Do you want to save results and exit match?",fg = "black", bg = "light blue")
    yes_button= tk.Button(save_match_w, text = "Yes", width = 6,height = 1, bg = "SpringGreen2", fg = "black", command = save)
    no_button= tk.Button(save_match_w, text = "No", width = 6,height = 1, bg = "tomato2", fg = "black", command = destroy)
    
    lbl.place(x = 50, y = 10)
    yes_button.place(x = 120, y = 50)
    no_button.place(x = 220, y = 50)
    
    save_match_w.mainloop()

def player_settings():
    ############# Configuratie #############
    player_w = tk.Tk()
    
    player_w.title("Player settings")                               #rename title of window
    player_w.resizable(0, 0)                                          #Make not resizable
    
    player_w.geometry('300x500+1150+300')
    player_w.configure(bg = "light blue")                            #background color
    
    ############# Buttons #############
    button_w = 15
    button_h = 3
    #bg_color = "light blue"
    
    add_player = tk.Button(player_w, 
        text = "add player",
        width = button_w,
        height = button_h,
        fg = "black",
        command = lambda: main.add_player()
    )
    
    edit_player = tk.Button(player_w, 
        text = "edit player",
        width = button_w,
        height = button_h,
        fg = "black",
        command = lambda: main.edit_player()
    )
    
    edit_color = tk.Button(player_w, 
        text = "edit color",
        width = button_w,
        height = button_h,
        command = lambda: main.edit_player_color()
    )
    
    color_test = tk.Button(player_w, 
        text = "color test",
        width = button_w,
        height = button_h,
        fg = "black",
        command= lambda: main.color_test()
    )
    
    cancel = tk.Button(player_w, 
        text = "Go back",
        width = button_w,
        height = button_h,
        bg = "tomato2",
        fg = "black",
        command = lambda: player_w.destroy()
    )
    
    ############# place #############
    add_player.place(relx=0.5, rely=(0.9/5)-0.05, anchor="center")
    edit_player.place(relx=0.5, rely=(2*(0.9/5))-0.05, anchor="center")
    edit_color.place(relx=0.5, rely=(3*(0.9/5))-0.05, anchor="center")
    color_test.place(relx=0.5, rely=(4*(0.9/5))-0.05, anchor="center")
    cancel.place(relx=0.5, rely=(0.9)-0.05, anchor= "center")
    
    player_w.mainloop()

start()
start_window()

    