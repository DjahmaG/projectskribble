import math
import sys
import os
from contender import Contender
from loop import Main

#clear terminal
os.system("cls")

#open csv file
f = open("ratings.csv", "r")
f.readline()

klassement = []
klassement_temp = {}

for x in f:
    (id, name, rating, color_codes) = x.split(';')
    #print(f.readline())
    klassement_temp[id] = (Contender(id, name, rating, color_codes))

for key in klassement_temp:
    klassement.append(None)

for key in klassement_temp:
    klassement[int(key)] = klassement_temp[key]

f.close()


main = Main()
main.klassement = klassement
print('')
main.print_klassement()

main.main_loop()

