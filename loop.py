from match import Match
from contender import Contender
from util.color import color_string_from_list
from util.title import Titles


class Main:
    functions = ["quit", "help", "add player", "leaderboard",
                 "edit player", "edit color", "colortest",
                 "titles",
                 "start match", "show match", "cancel match", "end match", "add berserk",
                 "report match",
                 "save results"]

    def __init__(self):
        self.klassement = []
        self.match = None

    def main_loop(self):
        Main.print_help()
        inp = ""
        while not inp.__eq__("quit"):
            inp = input()
            inp.strip()

            # switch:
            if inp.__eq__("colortest"):
                self.color_test()
            elif inp.startswith("leaderboard") or inp.__eq__("lb"):
                self.print_klassement()
            elif inp.startswith("start match") or inp.__eq__("start"):
                self.start_match()
            # niet gebruikt
            elif inp.startswith("show match") or inp.__eq__("sm"):
                self.show_match()
            elif inp.startswith("cancel match"):
                self.cancel_match()
            elif inp.startswith("end match"):
                self.end_match()
            elif inp.startswith("add player"):
                self.add_player()
            elif inp.startswith("add berserk") or inp.__eq__("ab"):
                self.set_berserk()
            elif inp.startswith("report match") or inp.__eq__("rm"):
                self.start_match()
                self.end_match()
                self.print_klassement()
            elif inp.startswith("save results"):
                self.save_results()
            elif inp.startswith("edit player"):
                self.edit_player()
            elif inp.startswith("edit color"):
                self.edit_player_color()
            elif inp.startswith("titles"):
                self.print_titles()
            elif inp.__eq__("quit"):
                self.save_results()
            elif inp.__eq__("cancel"):
                inp = "quit"
            elif not inp.__eq__(""):
                Main.print_help()

    def print_klassement(self):
        print(color_string_from_list(" {:7}{:20}{:}:".format("Id", "Naam", "Rating"), [7]))
        ranking = []
        for c in self.klassement:
            ranking.append(c)

        while len(ranking) > 0:
            highest = ranking[0]
            for c in ranking:
                if c.rating > highest.rating:
                    highest = c
            ranking.remove(highest)

            print(" {:<7}{:}{:5}".format(str(highest.id),
                                         highest.get_name_with_title_and_spacing(17),
                                         str(highest.rating)))
        print("\n")

    @staticmethod
    def print_titles():
        for title in Titles:
            print(title.value.get_full_title_with_color() +
                  " (" + title.value.get_title_with_color() + "): rating over " +
                  str(title.value.rating))
        print("\n")

    def start_match(self, id_list=None):
        self.match = Match()
        self.print_klassement()

        id_list = Main.__get_players_for_match() if id_list is None else id_list

        print(color_string_from_list("starting match...", [92]))

        for id in id_list:
            if int(id) >= len(self.klassement) or not id.isnumeric():
                print(id + " is wrong")
                continue
            self.match.add_contestant(self.klassement[int(id)])

    @staticmethod
    def __get_players_for_match():
        print('Enter id\'s of contenders and separate by colon (id1,id2,...)')
        inp = input()
        return inp.split(',')

    def show_match(self):
        if self.match is None:
            print(color_string_from_list("No match initialised.", [92]))
        else:
            print(color_string_from_list("Match with "
                  + str(self.match.amount_of_contestants())
                  + " contestants.", [92]))
            self.match.print_contestants()
            print("")

    # wordt niet meer gebruikt
    def set_berserk(self):
        if self.match is None:
            print(color_string_from_list("No match initialised.", [92]))
        else:
            print(color_string_from_list(" type id's of berserking players.", [92]))
            ids = input().strip().split(' ')
            for id in ids:
                if int(id) >= len(self.klassement) or not id.isnumeric():
                    print(id + " is wrong")
                    continue
                self.match.add_berserker(id)

    def cancel_match(self):
        if self.match is None:
            print(color_string_from_list("No match initialised.", [92]))
        else:
            print(color_string_from_list("Match with canceled", [92]))

    def save_results(self):
        f = open("ratings.csv", "w")
        f.write("Id;Naam;Rating;Colorcodes\n")
        for c in self.klassement:
            f.write(str(c.id) + ";" + c.name + ";" + str(c.rating) + ";" + c.get_color_codes() + "\n")
        print(color_string_from_list("results saved.", [95]))
        f.close()

    def end_match(self, scores=None):
        if self.match is None:
            print(color_string_from_list("No match initialised.", [92]))
        else:
            print(color_string_from_list("Scores:", [92]))
            self.match.enter_scores(scores)
            print('\n')

    def edit_player(self):
        print(color_string_from_list("reformat a player: \nid name rating cc1,cc2c...", 92))
        inp = input().strip().split(' ')
        if len(inp) < 4:
            print('\033[92m' + "Edit canceled." + '\033[0m')
        else:
            self.klassement[int(inp[0])] = Contender(int(inp[0]), inp[1], int(inp[2]), inp[3])
            self.print_klassement()

    def edit_player_color(self):
        print(color_string_from_list("reformat a player: \nid cc1,cc2c...", [92]))
        inp = input().strip().split(' ')
        if len(inp) < 2:
            print('\033[92m' + "Edit canceled." + '\033[0m')
        else:
            self.klassement[int(inp[0])].color_codes = inp[1].strip().split(',')
            self.print_klassement()

    def add_player(self):
        print(color_string_from_list("add a player: \nname cc1,cc2c...", [92]))
        inp = input().strip().split(' ')
        if len(inp) < 2:
            print(color_string_from_list("Adding canceled.", [92]))
        else:
            self.klassement.append(Contender(len(self.klassement), inp[0], 1500, inp[1]))
            self.print_klassement()

    @staticmethod
    def color_test():
        for i in range(0, 100):
            print(color_string_from_list("test " + str(i), [i]))
        print("\n\n")

    @staticmethod
    def print_help():
        print()
        print(color_string_from_list(
            "Use one of the following commands:",
            [92, 7]))
        c = 0
        print('\t', end='')
        for x in Main.functions:
            print(color_string_from_list(x, [92]), end=', ')
            if c % 3 == 2:
                print("\n\t", end='')
            c += 1
        print()
