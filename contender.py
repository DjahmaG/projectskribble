from util.color import color_string_from_list
from util.title import Titles


class Contender:

    def __init__(self, id, name, rating="1500", color_codes=None):
        if color_codes is None:
            color_codes = []
        self.id = id
        self.name = name
        self.rating = int(rating.strip())
        self.color_codes = color_codes.strip().split(',')

    def get_name(self):
        return color_string_from_list(self.name, self.color_codes)

    def get_name_with_title(self):
        title = Titles.get_title(self.rating)
        title_string = title.get_title_with_color() + ' ' if title is not None else "   "
        return title_string + self.get_name()

    def get_name_with_title_and_spacing(self, spacing=17):
        name = ''
        name += self.get_name_with_title()
        name += (spacing - self.name_length()) * " "
        return name

    def name_length(self):
        return len(self.name)

    def get_color_codes(self):
        codes = ""
        for x in self.color_codes:
            codes += x + ','
        return codes.rstrip(',')
